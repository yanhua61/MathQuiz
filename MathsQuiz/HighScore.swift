//
//  HighScores.swift
//  MathsQuiz
//
//  Created by Yanhua Li on 2/11/16.
//  Copyright © 2016 Yanhua Li. All rights reserved.
//

import Foundation
import Firebase

struct HighScore {
	let name : String
	let score: Int
	let difficulty: String
	
	init(snapshot : FIRDataSnapshot) {
		let snapshotValue = snapshot.value as! [String : AnyObject]
		self.name = snapshotValue["name"] as! String
		self.score = snapshotValue["score"] as! Int
		self.difficulty = snapshotValue["difficulty"] as! String
	}
	
	init(name: String, score: Int, difficulty: String) {
		self.name = name
		self.score = score
		self.difficulty = difficulty
	}
	
	func toAnyObject() -> Any {
		return ["name" : self.name, "score" : self.score, "difficulty" : self.difficulty]
	}
	
	
}
