//
//  EnterNameViewController.swift
//  MathsQuiz
//
//  Created by Yanhua Li on 6/11/16.
//  Copyright © 2016 Yanhua Li. All rights reserved.
//

import UIKit

class EnterNameViewController: UIViewController {

	@IBOutlet weak var NameTextField: UITextField!
	
	var name: String!
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

	
	@IBAction func NameTextFieldDidEndOnExit(_ sender: UITextField) {
		self.name = sender.text!
		performSegue(withIdentifier: "ToDifficulty", sender: self)
	}

	@IBAction func ContinueAsAnonymousTapped(_ sender: UIButton) {
		self.name = "Anonymous"
		performSegue(withIdentifier: "ToDifficulty", sender: self)
	}


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! DifficultyViewController
		destinationVC.name = self.name
    }

}
