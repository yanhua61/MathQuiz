//
//  EntryViewController.swift
//  MathsQuiz
//
//  Created by Yanhua Li on 1/11/16.
//  Copyright © 2016 Yanhua Li. All rights reserved.
//

import Foundation
import UIKit

class DifficultyViewController : UIViewController {
	var difficulty: QuestionDifficulty!
	var name: String!
	
	@IBOutlet var Buttons: [UIButton]!
	@IBOutlet var NameLabel: UILabel!
	

	override func viewDidLoad() {
		self.NameLabel.text = self.name
		
	}
	
	
	
	@IBAction func buttonTapped(_ sender: UIButton) {
		switch sender.currentTitle! {
		case "Easy" :
			self.difficulty = QuestionDifficulty.easy
		case "Medium" :
			self.difficulty = QuestionDifficulty.medium
		case "Hard" :
			self.difficulty = QuestionDifficulty.hard
		default: break
		}
		
		performSegue(withIdentifier: "SegueToGame", sender: self)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let destinationVC = segue.destination as! GameViewController
		destinationVC.difficulty = self.difficulty
		destinationVC.name = self.name
	}
}
