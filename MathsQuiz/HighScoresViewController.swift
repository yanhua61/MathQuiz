//
//  HighScoresViewController.swift
//  MathsQuiz
//
//  Created by Yanhua Li on 2/11/16.
//  Copyright © 2016 Yanhua Li. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class HighScoresViewController : UIViewController, UITableViewDelegate, UITableViewDataSource{
	
	@IBOutlet weak var HighScoresTableView: UITableView!
	
	@IBOutlet weak var PlayAgainButton: UIButton!
	
	@IBOutlet weak var DifficultySegmentedControl: UISegmentedControl!
	
	var name : String!
	
	var easyHighScores : [HighScore]?
	
	var mediumHighScores : [HighScore]?
	
	var hardHighScores : [HighScore]?
	
	var difficulty : String!
	



	override func viewDidLoad() {
		
		listenForDataChanges()
		
		switch self.difficulty {
			case "medium":
				self.DifficultySegmentedControl.selectedSegmentIndex = 1
			case "hard":
				self.DifficultySegmentedControl.selectedSegmentIndex = 2
		default:
			break
		}
		
	}
	
	@IBAction func PlayAgainButtonTapped(_ sender: UIButton) {
		performSegue(withIdentifier: "PlayAgain", sender: self)
	}
	
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let destinationVC = segue.destination as! DifficultyViewController
		destinationVC.name = self.name
	}
	
	func listenForDataChanges() {
		
		
		let ref = FIRDatabase.database().reference().child("high-scores")
		
		ref.queryOrdered(byChild: "score").observe(.value, with: { snapshot in
			
			self.easyHighScores = []
			self.mediumHighScores = []
			self.hardHighScores = []
			
			for item in snapshot.children.reversed() {
				let myItem = item as! FIRDataSnapshot
				let highScore = HighScore(snapshot: myItem)
				if highScore.difficulty == "easy" {
					self.easyHighScores?.append(highScore)
				} else if highScore.difficulty == "medium" {
					self.mediumHighScores?.append(highScore)
				} else {
					self.hardHighScores?.append(highScore)
				}
			
			}
			
			self.HighScoresTableView.reloadData()

		})
	}
	
	@IBAction func SegmentedControlValueChanged(_ sender: UISegmentedControl) {
		switch sender.selectedSegmentIndex {
		case 0:
			self.difficulty = "easy"
		case 1:
			self.difficulty = "medium"
		case 2:
			self.difficulty = "hard"
		default:
			break

		}
		
		self.HighScoresTableView.reloadData()
	}
	
	
	

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let easyCount = self.easyHighScores?.count else {
			return 0
		}
		guard let mediumCount = self.mediumHighScores?.count else {
			return 0
		}
		guard let hardCount = self.hardHighScores?.count else {
			return 0
		}
		
		switch self.difficulty {
			case "easy":
				return easyCount
			case "medium":
				return mediumCount
			case "hard":
				return hardCount
			
		default:
			return 0
		}
		
		
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = HighScoresTableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
		if self.difficulty == "easy" {
			cell.textLabel?.text = easyHighScores![indexPath.row].name
			cell.detailTextLabel?.text = String(easyHighScores![indexPath.row].score)
		} else if self.difficulty == "medium" {
			cell.textLabel?.text = mediumHighScores![indexPath.row].name
			cell.detailTextLabel?.text = String(mediumHighScores![indexPath.row].score)
		} else {
			cell.textLabel?.text = hardHighScores![indexPath.row].name
			cell.detailTextLabel?.text = String(hardHighScores![indexPath.row].score)
		}
		return cell
	}
	
	
}
