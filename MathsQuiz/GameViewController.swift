//
//  ViewController.swift
//  MathsQuiz
//
//  Created by Yanhua Li on 30/10/16.
//  Copyright © 2016 Yanhua Li. All rights reserved.
//

import UIKit
import Firebase

class GameViewController: UIViewController {
    
	var timer : Timer!
	
    var count = 60
    
    var score = 0
    
    var question : Question!
	
	var name: String!

    @IBOutlet weak var QuestionLabel: UILabel!
    
    @IBOutlet weak var TimeLabel: UILabel!
    
    @IBOutlet weak var ScoreLabel: UILabel!
    
    @IBOutlet weak var AnswerButton1: UIButton!
    @IBOutlet weak var AnswerButton2: UIButton!
    @IBOutlet weak var AnswerButton3: UIButton!
    @IBOutlet weak var AnswerButton4: UIButton!
	
	@IBOutlet weak var NameLabel: UILabel!

	
	var AnswerButtons : [UIButton]!
	
	var difficulty : QuestionDifficulty!

    

    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		setUpButtons()
		
        startGame()
        
        setUpTimer()
		
	}
    
    func setUpTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeLabel), userInfo: nil, repeats: true)
    }
	
	func setUpButtons() {
		self.AnswerButtons = [AnswerButton1, AnswerButton2, AnswerButton3, AnswerButton4]
		
		self.NameLabel.text = self.name
		
		for button in self.AnswerButtons {
			button.layer.cornerRadius = 5
			button.layer.borderWidth = 1
			button.contentEdgeInsets = UIEdgeInsetsMake(5, 15, 5, 15)
		}
	}
    
    func startGame() {
		
		self.question = Question(difficulty: self.difficulty)
        
		
            
		for button in AnswerButtons {
			let wrongAnswer = question.wrongAnswers.popFirst()
			button.setTitle(String(wrongAnswer!), for: UIControlState())
		}
		
		
		QuestionLabel.text = question.getQuestion()
		
		let correctAnswerButton = Int(arc4random_uniform(4) + 1)
		
		switch correctAnswerButton {
		case 1:
			AnswerButton1.setTitle(String(question.answer), for: UIControlState())
		case 2:
			AnswerButton2.setTitle(String(question.answer), for: UIControlState())
		case 3:
			AnswerButton3.setTitle(String(question.answer), for: UIControlState())
		case 4:
			AnswerButton4.setTitle(String(question.answer), for: UIControlState())
		default:
			break
		}
		
    }
	

	
    @IBAction func AnswerButtonTapped(_ sender: UIButton) {
        if sender.currentTitle == String(question.answer) {
            score = score + 10
            ScoreLabel.text = String(score)
            
		} else {
			score = score - 10
			ScoreLabel.text = String(score)
		}
			
        startGame()
    }
    
    func updateTimeLabel() {
        
        count = count - 1
        TimeLabel.text = String(count)
        
        if count == 0 {
			self.timer.invalidate()
			saveHighScore()
			performSegue(withIdentifier: "ToHighScores", sender: nil)
        }
        
    }
	
	func saveHighScore() {
		let highScore = HighScore(name: self.name, score: self.score, difficulty: self.difficulty.asString)
		let ref = FIRDatabase.database().reference(withPath: "high-scores")
		
		ref.childByAutoId().setValue(highScore.toAnyObject())
		
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let destinationVC = segue.destination as! HighScoresViewController
		destinationVC.name = self.name
		destinationVC.difficulty = self.difficulty.asString
		
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

