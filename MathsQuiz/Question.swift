//
//  Question.swift
//  MathsQuiz
//
//  Created by Yanhua Li on 30/10/16.
//  Copyright © 2016 Yanhua Li. All rights reserved.
//

import Foundation

enum QuestionDifficulty {
	case easy
	case medium
	case hard
	
	var asString : String {
		switch self {
		case .easy:
			return "easy"
		case .medium:
			return "medium"
		case.hard:
			return "hard"
		}
	}
}

class Question {
    
    var difficulty: QuestionDifficulty
    var answer: Int
	var wrongAnswers: Set<Int>
    var deviance: Int
    var operand1: Int
    var operand2: Int
    
    init(difficulty: QuestionDifficulty) {
		self.wrongAnswers = Set<Int>()
        self.difficulty = difficulty
        self.deviance = 0
        self.answer = 0
        self.operand1 = 0
        self.operand2 = 0
		
        switch self.difficulty {
            case .easy:
                self.deviance = 10
                self.operand1 = Int(arc4random_uniform(10)) + 1
                self.operand2 = Int(arc4random_uniform(10)) + 1
                self.answer = (self.operand1 + self.operand2)
            
            case .medium:
                self.deviance = 20
                self.operand1 = Int(arc4random_uniform(50)) + 1
                self.operand2 = Int(arc4random_uniform(50)) + 1
                self.answer = (self.operand1 + self.operand2)
            
            case .hard:
                self.deviance = 50
                self.operand1 = Int(arc4random_uniform(100)) + 1
                self.operand2 = Int(arc4random_uniform(100)) + 1
                self.answer = (self.operand1 + self.operand2)
        }
        
        generateWrongAnswers()
    }
    
    func generateWrongAnswers() {
		let lowerBound = self.answer - self.deviance
		let upperBound = self.answer + self.deviance
		let randomDifference = UInt32(upperBound - lowerBound)
		
		while self.wrongAnswers.count < 4 {
			let wrongAnswer : Int = Int(arc4random_uniform(randomDifference)) + lowerBound
			
			if wrongAnswer != self.answer && wrongAnswer > 0 {
				wrongAnswers.insert(wrongAnswer)
			}
			
		}
        
    }
    
    func getQuestion() -> String {
        let question = String(operand1) + " + " + String(operand2)
        
        return question
    }
}

